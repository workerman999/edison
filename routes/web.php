<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/get-psyhics', 'HomeController@getPsyhics');
Route::get('/answer-psyhic', 'HomeController@getAnswerPsyhic');
Route::post('/answer-user', 'HomeController@setAnswerUser');
