<?php

namespace App\Models;

class Psychic
{
    public $name;
    public $variant;
    public $correctAnswer = 0;
    public $totalRequests = 0;
    public $rating = 0;
    public $img;

    public function __construct($name)
    {
        $this->name = $name;
        $this->img = "/img/" . $name . ".jpg";
    }
}
