<?php

namespace App\Http\Controllers;

use App\Models\Psychic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    public function index()
    {
        return view('index');
    }

    /** Получаем экстрасенсов */
    public function getPsyhics()
    {
        if (Session::has('psyhics')){
            $psyhics = Session::get('psyhics');
            return $psyhics;
        }

        $gecati = new Psychic('Константин Гецати');
        $raidos = new Psychic('Виктория Райдос');
        $dashi = new Psychic('Свами Даши');

        $psyhics = [];
        $psyhics[] = $gecati;
        $psyhics[] = $raidos;
        $psyhics[] = $dashi;

        Session::put('psyhics', $psyhics);

        return $psyhics;
    }

    /** Получаем предположения экстрасенсов */
    public function getAnswerPsyhic()
    {
        if (Session::has('psyhics')){
            $psyhics = Session::get('psyhics');
        } else {
            return false;
        }

        foreach ($psyhics as $psyhic){
            $psyhic->variant = $this->getVariant();
        }
        return $psyhics;
    }

    /** Проверяем совпадение и считаем уровень достоверности каждого экстрасенса */
    public function setAnswerUser(Request $request)
    {
        if (Session::has('psyhics')){
            $psyhics = Session::get('psyhics');
        } else {
            return false;
        }

        foreach ($psyhics as $psyhic){
            if ($psyhic->variant == (int)$request->number) {
                $psyhic->correctAnswer++;
                $psyhic->totalRequests++;
            } else {
                $psyhic->totalRequests++;
            }

            $psyhic->rating = $this->getRaiting($psyhic->correctAnswer, $psyhic->totalRequests);
        }
        return $psyhics;
    }

    /** Получение рандомного числа */
    public function getVariant()
    {
        return mt_rand(10, 99);
    }

    /** Подсчет рейтинга */
    public function getRaiting($correctAnswer, $totalRequests)
    {
        return round($correctAnswer / $totalRequests * 100);
    }
}
